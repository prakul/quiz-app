-- phpMyAdmin SQL Dump
-- version 3.3.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 05, 2011 at 06:16 PM
-- Server version: 5.1.54
-- PHP Version: 5.3.5-1ubuntu7.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quiz`
--

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE IF NOT EXISTS `answer` (
  `user_id` varchar(30) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `q_no` int(2) NOT NULL,
  `user_ans` char(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_add` varchar(15) NOT NULL,
  `points` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answer`
--


-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `first_name` varchar(25) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(36) NOT NULL,
  `Institute` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--


-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE IF NOT EXISTS `question` (
  `q_no` int(11) NOT NULL,
  `q` varchar(60) NOT NULL,
  `op_1` varchar(15) NOT NULL,
  `op_2` varchar(15) NOT NULL,
  `op_3` varchar(15) NOT NULL,
  `op_4` varchar(15) NOT NULL,
  `r_ans` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`q_no`, `q`, `op_1`, `op_2`, `op_3`, `op_4`, `r_ans`) VALUES
(1, 'What was code name for windows xp', 'longhorn', 'memphis', 'whistler', 'shrimp', '3'),
(2, 'who was the photographer at larry ellison''s wedding?', 'bill gates', 'steve jobs', 'warren buffet', 'john trump', '2'),
(3, 'test', 'test1', 'test2', 'test', 'test3', '3'),
(4, 'hey', 'hey1', 'hey2', 'hey3', 'hey', '4'),
(5, 'hii', 'hii1', 'hii', 'hii2', 'hii3', '2');
